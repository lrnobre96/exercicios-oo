package Banco;

import java.util.Scanner;

public class Banco {
	
	private static int indice;
	private Conta[] contas;
	private int maxConta = 5;
	
	public Banco() {
        contas = new Conta[maxConta];
        indice = 0;
    }
	
	static Scanner leia = new Scanner(System.in);
	
	public void cadastrar(Conta c) {
		if (indice < maxConta) {
			contas[indice] = c;
			indice += 1;
			System.out.println("cadastro realizado");
		}
		else {
			System.out.println("limite de contas alcan�ado");
		}
	}
	
	private Conta procurar(long numero) {
		int i= 0;
		boolean achou = false;
		Conta retorno;
		
		while ( (!achou) && (i<indice) ) {
			if (contas[i].getNumero() == numero) 
				achou = true;
			
			else 
				i+=1;
			
		}
			
			if (achou == true) 
				retorno = contas[i];
			
			else
				retorno = null;
			
			return retorno;
		}
	
	public void excluir(long numero) {
		Conta c;
		c = procurar(numero);
		if (c == null) {
			System.out.println("Conta n�o existe");
		}
		else {
			int i = 0;
			for (i =0; i <indice ; i++) {
				contas[i] = new Conta(0,0);
				System.out.println("Contas Excluidas");
			}
		}
	}
	
	public void printSaldo (long numero ) {
		Conta c;
		c= procurar(numero);
		if ( c == null) {
			System.out.println("Conta n�o existe");
		}
		else {
			System.out.println("O Saldo �" + c.getSaldo());
		}
	}
	
	public void sacar(long numero, double valor) {
		Conta c;
		c = procurar (numero);
		if (c == null)
			System.out.println("Conta n�o existe");
		else {
			c.sacar(valor);
			System.out.println("Saque realizado");
		}
	}
	
	public void depositar(long numero, double valor) {
		Conta c;
		c = procurar (numero);
		if (c == null)
			System.out.println("Conta n�o existe");
		else {
			c.depositar(valor);
			System.out.println("deposito realizado");
		}
	}
	
	public void transferencia(long numeroFrom, long numeroTo,double valor) {
		Conta c,c2;
		c =procurar (numeroFrom); c2 = procurar(numeroTo);
		if (c == null)
			System.out.println("Conta de saida n�o existe");
		else if (c2 == null)
			System.out.println("Conta destinaria n�o existe");
		else
		{
			c.sacar(valor);
			c2.depositar(valor);
			System.out.println("Valor transferido");
		}
	}
}
