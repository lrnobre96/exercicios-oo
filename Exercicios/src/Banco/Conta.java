package Banco;

public class Conta {
	private long numero;
	private double saldo;
	
	public Conta (long n, double s) 
	{
		numero = n;
		saldo = s;
	}
	
	public long getNumero() {
		return numero;
	}
	
	
	public void setNumero(long numero)
	{
		this.numero = numero;
	}
	
	public double getSaldo () {
		return saldo;
	}
	
	public void setSaldo (double saldo) {
		this.saldo = saldo;
	}
	
	public void sacar (double valor) {
		saldo -= valor;
	}
	
	public void depositar (double valor) {
		saldo += valor;
	}
	
}
