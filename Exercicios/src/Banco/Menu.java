package Banco;

import java.util.Scanner;

public class Menu {
	//Cria o banco
    static Banco banco = new Banco();
    
    //Objeto para ler informa��es do teclado
    static Scanner leia = new Scanner(System.in);
    
    public static void main (String[] args) {
    	int op;
    	do {
    		System.out.println("::: MENU PRINCIPAL :::");
            System.out.println("1-Cadastrar Conta");
            System.out.println("2-Excluir Conta");
            System.out.println("3-Saldo da Conta");
            System.out.println("4-Debitar");
            System.out.println("5-Creditar");
            System.out.println("6-Transferir");
            System.out.println("7-Sair");
            System.out.println("Digite sua op��o: ");
            op = leia.nextInt(); 
            switch(op){
                case 1: incluirConta(); break;
                case 2: excluirConta(); break;
                case 3: imprirSaldoDaConta(); break;
                case 4: debitar(); break;
                case 5: creditar(); break;
                case 6: transferir(); break;
                case 7: break;
               
            }
        } while (op!=7);
    }
    
    public static void incluirConta(){
        //Entrada de dados
        System.out.println("Digite o n�mero da conta:");
        long num = leia.nextLong();
        System.out.println("Digite o saldo da conta:");
        double saldo = leia.nextDouble();
        //Cadastro da conta no banco
        banco.cadastrar(new Conta(num, saldo));
    }
    
    public static void excluirConta(){
        System.out.println("Digite o n�mero da conta:");
        long num = leia.nextLong();
        banco.excluir(num);
    }
    
    public static void imprirSaldoDaConta() {
    	System.out.println("digite o numero da conta");
    	long num = leia.nextLong();
    	banco.printSaldo(num);
    }
    
    public static void debitar() {
    	System.out.println("digite o numero da conta");
    	long num = leia.nextLong();
    	System.out.println("Digite o valor a ser debitado da conta:");
        double valor = leia.nextDouble();
        banco.sacar(num, valor);
    }
    
    public static void creditar() {
    	System.out.println("digite o numero da conta");
    	long num = leia.nextLong();
    	System.out.println("Digite o valor a ser creditado na conta:");
        double valor = leia.nextDouble();
        banco.depositar(num, valor);
    }
    
    public static void transferir() {
    	System.out.println("digite o numero da conta de origem");
    	long num = leia.nextLong();
    	System.out.println("Digite o valor a ser transferido da conta:");
        double valor = leia.nextDouble();
        System.out.println("digite o numero da conta destino ");
    	long num2 = leia.nextLong();
    	banco.transferencia(num, num2, valor);
    }
    
    

    
    


}
