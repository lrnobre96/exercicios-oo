package Elei��o;

public class Candidato {
	private int numero, votos;
	private String nome;
	
	public Candidato(int n,String no) {
		this.numero = n;
		this.votos = 0;
		this.nome = no;
	}
	
	public int getNumero() {
		return numero;
	}
	
	public int getVotos() {
		return votos;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	public void setVotos(int votos) {
		this.votos = votos;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void incrementarVoto()
	{
		this.votos += 1;
	}
}
