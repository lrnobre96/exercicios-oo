package Estoque_Simples;

import java.util.Scanner;

public class Estoque {
	static Scanner leia = new Scanner(System.in);
	static Produto produto[] = new Produto[3];
	
	public static void main (String[] args) {
		int op= 0;
		do {
			System.out.println("::: MENU PRINCIPAL :::");
			System.out.println("1- Cadastrar produtos");
			System.out.println("2 - Listar Produtos ");
			System.out.println("3- Verificar Estoque");
			System.out.println("4- Remover Produto ");
			System.out.println("5- Sair ");
			op = leia.nextInt();
				switch(op){
				case 1: cadastrar();break;
				case 2: listar();break;
				case 3: verificar();break;
				case 4: remover();break;
				case 5: break;
			}
		} while (op != 4);
	}
	
	public static void cadastrar () {
		for (int i=0; i<3; i++) {
			System.out.println("Escreva o nome do produto:");
			String n = leia.next();
			System.out.println("Escreva o pre�o do produto:");
			double p = leia.nextDouble();
			System.out.println("Escreva a quantidade do produto:");
			int q = leia.nextInt();
			produto[i]= new Produto(n,p,q);
		}
	}
	
	public static void listar() {
		for (int i=0; i<3; i++) {
			System.out.println("Nome: "+ produto[i].getNome() + "       "+ "Pre�o: "+ produto[i].getPre�o()+"       "+
								"Quantidade: "+ produto[i].getQuantidade() );
		}
	}
	
	public static void verificar() {
		int min= 5;
		for (int i=0; i<3; i++) {
			if (produto[i].getQuantidade()< min ) {
				System.out.println("O produto "+ produto[i].getNome()+ " est� com poucas unidades em estoque" );
			}
		}
	}
	
	public static void remover () {
		System.out.println("Qual o nome do produto a ser retirado");
		String n = leia.next();
		for (int i =0; i<3 ; i++) {
			if (produto[i].getNome() == n) {
				produto[i] = new Produto(n=null,0,0);
				System.out.println("removido com sucesso");
			}
		}
	}
}
